import json
import random

import pulp
import pandas

names = ['pepsi', 'coke', 'sprite', 'dew', 'seltzer']

def read_file():
    with open('gift.json') as f:
        data = f.read()
    return data


def generate_random_data():

    product_info = []
    columns = ['product_name', 'price', 'ratings', 'quantity']
    for i in range(0, 50):
        product_info.append([random.choice(names), random.uniform(3, 5), random.randint(0, 5), random.randint(9, 12)])

    return product_info, columns


def get_ratings():

    data, columns = generate_random_data()
    df = pandas.DataFrame(data, columns=columns)

    df['unit_price'] = df['price'] / df['quantity']

    return df


def form_lp(df, budget, number_of_soda):

    # create a LP
    prob = pulp.LpProblem("The Amazon Problem", pulp.LpMinimize)
    df['variables'] = df.apply(lambda r: pulp.LpVariable(str(r.name), cat='Binary'), axis=1)

    # define objective function
    prob += pulp.lpSum(df['unit_price'] * df['quantity'])
    prob += pulp.lpSum(- df['ratings'])

    # define constraints
    prob += pulp.lpSum(df['price'] * df['variables']) <= budget
    prob += pulp.lpSum(df['quantity'] * df['variables']) <= number_of_soda + 0.1
    prob += pulp.lpSum(df['quantity'] * df['variables']) >= number_of_soda - 0.1

    #solve
    prob.solve(pulp.COIN_CMD(path='cbc'))

    df['outcome'] =  df['variables'].apply(lambda x : x.varValue)

    choosen_values = df[df.outcome==1]

    print choosen_values

def main():
    df = get_ratings()
    form_lp(df, 20, 30)

main()
