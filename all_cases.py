import json
import random

import pulp
import pandas

names = ['pepsi', 'coke', 'sprite', 'dew', 'seltzer']

def read_file():
    with open('gift.json') as f:
        data = f.read()
    return data


def generate_random_data():

    product_info = []
    columns = ['product_name', 'price', 'ratings', 'quantity']
    for i in range(0, 50):
        product_info.append([random.choice(names), random.uniform(3, 5), random.randint(0, 5), random.randint(9, 12)])

    return product_info, columns


def get_ratings():

    data, columns = generate_random_data()
    df = pandas.DataFrame(data, columns=columns)

    df['unit_price'] = df['price'] / df['quantity']

    return df


def min_budget_max_ratings(prob, budget, number_of_soda):

    # define objective function
    prob += pulp.lpSum(df['unit_price'] * df['quantity']  * df['variables'])
    prob += pulp.lpSum(- df['ratings']  * df['variables'])

    # define constraints
    prob += pulp.lpSum(df['price'] * df['variables']) <= budget
    prob += pulp.lpSum(df['quantity'] * df['variables']) <= number_of_soda + 5
    prob += pulp.lpSum(df['quantity'] * df['variables']) >= number_of_soda - 5

    return prob


def max_ratings(prob, budget, number_of_soda):

    # define objective function
    prob += pulp.lpSum(- df['ratings']  * df['variables'])

    # define constraints
    prob += pulp.lpSum(df['price'] * df['variables']) <= budget
    prob += pulp.lpSum(df['quantity'] * df['variables']) <= number_of_soda + 5
    prob += pulp.lpSum(df['quantity'] * df['variables']) >= number_of_soda - 5

    return prob


def min_budget(prob, budget, number_of_soda):
    # define objective function
    prob += pulp.lpSum(df['unit_price'] * df['quantity']  * df['variables'])
    # define constraints
    prob += pulp.lpSum(df['price'] * df['variables']) <= budget
    prob += pulp.lpSum(df['quantity'] * df['variables']) <= number_of_soda + 5
    prob += pulp.lpSum(df['quantity'] * df['variables']) >= number_of_soda - 5
    return prob


def just_ratings(prob, number_of_soda):

    # define objective function
    prob += pulp.lpSum(- df['ratings']  * df['variables'])

    prob += pulp.lpSum(df['quantity'] * df['variables']) <= number_of_soda + 5
    prob += pulp.lpSum(df['quantity'] * df['variables']) >= number_of_soda - 5

    return prob

def form_lp(df, budget, number_of_soda, case):

    # create a LP
    prob = pulp.LpProblem("The Amazon Problem", pulp.LpMinimize)
    df['variables'] = df.apply(lambda r: pulp.LpVariable(str(r.name), cat='Binary'), axis=1)

    print case
    if case == 'min_budget_max_ratings':
        min_budget_max_ratings(prob, budget, number_of_soda)

    elif case == 'max_ratings':
        max_ratings(prob, budget, number_of_soda)

    elif case == 'min_budget':
        min_budget(prob, budget, number_of_soda)

    elif case == 'just_ratings':
        just_ratings(prob, number_of_soda)

    #solve
    prob.solve(pulp.PULP_CBC_CMD())

    df['outcome'] =  df['variables'].apply(lambda x : x.varValue)

    choosen_values = df[df.outcome==1]
    print "cost = {s}".format(s=choosen_values.price.sum())
    print "ratings = {r}".format(r=choosen_values.ratings.mean())
    print choosen_values


df = get_ratings()

form_lp(df, 20, 30, "just_ratings")
form_lp(df, 20, 30, 'min_budget')
form_lp(df, 20, 30, 'max_ratings')
form_lp(df, 20, 30, 'min_budget_max_ratings')
